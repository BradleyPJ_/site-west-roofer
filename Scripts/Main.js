$(function() {
    $('.about-message img:not(:first)').hide();

    $('.about-message img').each(function() {
        var img = $(this);
        $('<img>').attr('src', $(this).attr('src')).load(function() {

        });
    });

    var pause = false;

    function fadeNext() {
        $('.about-message img').first().hide().appendTo($('.about-message'));
        $('.about-message img').first().fadeIn();
    }

    function fadePrev() {
        $('.about-message img').first().hide();
        $('.about-message img').last().prependTo($('.about-message')).fadeIn();
    }

    $('.about-message, #next').click(function() {
        fadeNext();
    });

    $('#prev').click(function() {
        fadePrev();
    });

    $('.about-message, .button').hover(function() {
        pause = true;
    },function() {
        pause = false;
    });

    function doRotate() {
        if(!pause) {
            fadeNext();
        }
    }

    var rotate = setInterval(doRotate,1000);
});
